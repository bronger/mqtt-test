module gitlab.com/bronger/mqtt-test

go 1.22.0

require github.com/eclipse/paho.golang v0.20.0

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/net v0.17.0 // indirect
)
