FROM golang as go-builder

COPY mqtt-test/go.mod mqtt-test/go.sum /go/src/humidity/
WORKDIR /go/src/humidity
RUN go mod download

COPY mqtt-test /go/src/humidity/
RUN go build


FROM ubuntu:24.04

RUN apt-get update && apt-get dist-upgrade -y --no-install-recommends --autoremove && \
    apt-get install -y --autoremove \
    mosquitto \
    supervisor \
    && rm -rf /var/lib/apt/lists/*

COPY --from=go-builder /go/src/humidity/mqtt-test /usr/local/bin/humidity

COPY supervisord.conf /etc/supervisor/

ENTRYPOINT ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
