package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/url"
	"os/signal"
	"syscall"
	"time"

	"github.com/eclipse/paho.golang/autopaho"
	"github.com/eclipse/paho.golang/paho"
)

func main() {
	baseCtx, baseCtxStop := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	c := getMQTTClient(baseCtx, "tcp://127.0.0.1:1883", "mqtt-tester")
	go func() {
		for {
			if err := publishMQTT(baseCtx, c, "mqtt-tester/test", map[string]any{"value": 0.0}); err != nil {
				panic(err)
			}
			time.Sleep(10 * time.Millisecond)
		}
	}()
	<-baseCtx.Done()

	slog.Info("Shutting down …")
	<-c.Done()
	baseCtxStop()
	slog.Info("Shutdown completed")
}

func getMQTTClient(ctx context.Context, address, name string) (c *autopaho.ConnectionManager) {
	mqttURL, err := url.Parse(address)
	if err != nil {
		panic(fmt.Errorf("Failed to parse URL to MQTT broker: %w", err))
	}
	cliCfg := autopaho.ClientConfig{
		BrokerUrls:            []*url.URL{mqttURL},
		KeepAlive:             3600 * 18,
		SessionExpiryInterval: 60,
		OnConnectionUp: func(c *autopaho.ConnectionManager, connAck *paho.Connack) {
			slog.Info("mqtt connection up")
		},
		OnConnectError: func(err error) { slog.Warn("Error whilst attempting connection", "error", err) },
		ClientConfig: paho.ClientConfig{
			ClientID:      name,
			OnClientError: func(err error) { slog.Warn("Server requested disconnect", "error", err) },
			OnServerDisconnect: func(d *paho.Disconnect) {
				if d.Properties != nil {
					slog.Info("Server requested disconnect", "reason", d.Properties.ReasonString)
				} else {
					slog.Info("Server requested disconnect", "reason code", d.ReasonCode)
				}
			},
		},
	}
	c, err = autopaho.NewConnection(ctx, cliCfg)
	if err != nil {
		panic(fmt.Errorf("autopaho.NewConnection returned an error (%w).  API change?", err))
	}
	return
}

func publishMQTT(ctx context.Context, c *autopaho.ConnectionManager, topic string, data any) error {
	msg, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}
	if err := c.AwaitConnection(ctx); err != nil {
		// err can only be the cancelled context
		return err
	}
	pr, err := c.Publish(ctx, &paho.Publish{
		Topic:   topic,
		Payload: msg,
	})
	if err != nil {
		return fmt.Errorf("Error publishing to MQTT topic “%s”: %w", topic, err)
	} else if pr != nil && pr.ReasonCode != 0 && pr.ReasonCode != 16 {
		// Reason 16 is that there are no subscribers
		return fmt.Errorf("Error publishing to MQTT topic “%s”: Reason code “%v” received", topic, pr.ReasonCode)
	} else {
		slog.Debug("Sent MQTT message successfully", "message", string(msg), "topic", topic)
		return nil
	}
}
