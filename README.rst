Paho issue #247
===============

This repo refers to <https://github.com/eclipse/paho.golang/issues/247>.


Running the test
----------------

You run the test with::

  ./run-test.sh

Then, wait for a couple of seconds.  If you can reproduce the issue, you then
see the following error messages::

  panic: Error publishing to MQTT topic “mqtt-tester/test”: \
  writev tcp 127.0.0.1:50802->127.0.0.1:1883: \
  use of closed network connection


What happens?
-------------

A docker image with the name ``mqtt-test`` is built and run as a container in
the foreground.  Within the container, three processes are started in parallel
by supervisord:

1. mosquitto
2. mqtt-test; its log output is sent to stdout
3. mqtt-test; its log output is sent to stderr

The script ``run-test.sh`` combines stdout and stderr of the container and greps
for „closed network“.

``mqtt-test`` is a Go program the source code of which resides in the sub
directory ``mqtt-test/``.  It creates an MQTT client with the client ID
``mqtt-test`` and publishes a lot of boring MQTT messages (not retained, QoS 0)
with a pause of 10 ms in between.  This eventually triggers a race that results
in the above error message.  It is a panic that makes the program abort.
supervisord restarts it then.

The main problem is the client ID.  Because both processes use the very same
one, mosquitto perpetually closes connections.
