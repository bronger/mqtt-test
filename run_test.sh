#!/bin/sh
set -e

docker build -t mqtt-test .
docker run --rm mqtt-test 2>&1 | grep "closed network"
